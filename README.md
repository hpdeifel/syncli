# syncli

syncli is a command line client for the synapse admin API.

## Development Status

This project is not maintained any more. There's a more feature complete and
actively developed synapse client at:

https://github.com/JOJ0/synadm

## Example

    $ syncli version
    server version: 1.12.0
    python version: 3.6.10
    
## Installation

1. Clone the git repository
2. Build the tool with `cargo build --release`
3. Copy `target/release/syncli` somewhere in your `PATH`

## Configuration

Configuration is stored in `~/.config/syncli/config.yaml`. It should look like
the following:

```yaml
access_token: SECRETADMINTOKEN
api_url: https://example.com/_synapse/admin
```

## Usage

See the output of `syncli --help`
