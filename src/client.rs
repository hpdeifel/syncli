use crate::Config;
use async_trait::async_trait;
use reqwest;
use serde::de::DeserializeOwned;
use serde::Deserialize;
use std::error::Error;
use std::fmt;

#[derive(Deserialize, Debug)]
pub struct SynapseError {
    errcode: String,
    error: String,
}

impl SynapseError {
    pub fn errcode(&self) -> &str {
        &self.errcode
    }

    pub fn error(&self) -> &str {
        &self.error
    }
}

impl fmt::Display for SynapseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "{}", self.error)
    }
}

impl Error for SynapseError {}

#[derive(Deserialize, Debug)]
#[serde(untagged)]
pub enum SynapseResponse<T> {
    Err(SynapseError),
    Ok(T),
}

impl<T> SynapseResponse<T> {
    pub fn into_result(self) -> Result<T, SynapseError> {
        match self {
            SynapseResponse::Ok(t) => Ok(t),
            SynapseResponse::Err(e) => Err(e),
        }
    }
}

pub struct Client {
    config: Config,
    client: reqwest::Client,
}

impl Client {
    pub fn new(config: Config) -> Self {
        let client = reqwest::Client::new();
        Client { config, client }
    }

    pub fn get(&self, endpoint: &[&str]) -> reqwest::RequestBuilder {
        let mut url = self.config.api_url().clone();
		{
			let mut segments = url.path_segments_mut().unwrap();
			for segment in endpoint {
				segments.push(&segment);
			}
		}
        self.client
            .get(url)
            .bearer_auth(self.config.access_token())
    }

    pub fn post(&self, endpoint: &[&str]) -> reqwest::RequestBuilder {
        let mut url = self.config.api_url().clone();
		{
			let mut segments = url.path_segments_mut().unwrap();
			for segment in endpoint {
				segments.push(&segment);
			}
		}
        self.client
            .post(url)
            .bearer_auth(self.config.access_token())
    }


    pub async fn get_synapse<'a, T: DeserializeOwned>(
        &self,
        endpoint: &[&str],
    ) -> Result<T, Box<dyn Error>> {
        self.get(endpoint).send().await?.synapse().await
    }
}

#[async_trait]
pub trait ResponseExt {
    async fn synapse<T: DeserializeOwned>(self) -> Result<T, Box<dyn Error>>;
}

#[async_trait]
impl ResponseExt for reqwest::Response {
    async fn synapse<T: DeserializeOwned>(self) -> Result<T, Box<dyn Error>> {
        Ok(self.json::<SynapseResponse<T>>().await?.into_result()?)
    }
}
