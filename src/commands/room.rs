use crate::*;
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use structopt::StructOpt;
use clap::arg_enum;

#[derive(StructOpt, Debug)]
pub enum RoomCmd {
    List(RoomListCmd),
}

#[derive(StructOpt, Serialize, Debug)]
pub struct RoomListCmd {
    #[structopt(long, help = "Offset of rooms to return")]
    from: Option<u64>,
	#[structopt(long, help = "Maximum amount of rooms to return")]
    limit: Option<u64>,
	#[structopt(long, help = "Sort order. Can be \"size\" or \"alphabetical\"")]
    order_by: Option<OrderBy>,
	#[structopt(long, help = "Sort direction. Can be \"f\" (forwards) or \"b\" (backwards)")]
    dir: Option<Direction>,
}

arg_enum! {
	#[derive(Serialize, Debug)]
	#[serde(rename_all="lowercase")]
	enum OrderBy {
		Alphabetical,
		Size,
	}
}

arg_enum! {
	#[derive(Serialize, Debug)]
	#[serde(rename_all="lowercase")]
	enum Direction {
		F,
		B,
	}
}

#[derive(Deserialize, Debug)]
struct Room {
    room_id: String,
    name: Option<String>,
    canonical_alias: Option<String>,
    // Why on earth can this be negative...
    joined_members: i64,
}

#[derive(Deserialize, Debug)]
struct RoomList {
    rooms: Vec<Room>,
    next_batch: Option<usize>,
	offset: usize,
	total_rooms: usize,
}

#[async_trait]
impl Command for RoomCmd {
    async fn run(self, client: &Client) -> Result<(), Box<dyn Error>> {
        match self {
            RoomCmd::List(opts) => {
                let res = client
                    .get(&["v1", "rooms"])
                    .query(&opts)
                    .send()
                    .await?
                    .synapse::<RoomList>()
                    .await?;
                for room in &res.rooms {
                    let alias = room.canonical_alias.as_deref().unwrap_or("-");
					let name = room.name.as_deref().unwrap_or("-");
                    println!(
                        "{}\t{}\t{}\t{:?}",
                        room.room_id, name, alias, room.joined_members
                    );
                }
				println!("Showing rooms {}-{} from {}", res.offset+1, res.offset + res.rooms.len(), res.total_rooms);
				if let Some(token) = res.next_batch {
					println!("use --from {} to get the next batch", token);					
				}
                Ok(())
            }
        }
    }
}
