use crate::*;
use async_trait::async_trait;
use structopt::StructOpt;
use serde::{Serialize, Deserialize};
use chrono::{DateTime, Utc};
use chrono::serde::ts_milliseconds;

#[derive(StructOpt, Serialize, Debug)]
pub struct PurgeMediaCacheCmd {
	#[serde(with = "ts_milliseconds")]
	before_ts: DateTime<Utc>
}

#[derive(Deserialize, Debug)]
struct PurgeResult {
	deleted: u64
}

#[async_trait]
impl Command for PurgeMediaCacheCmd {
	async fn run(self, client: &Client) -> Result<(), Box<dyn Error>> {
		let res = client
    		.post(&["v1", "purge_media_cache"])
    		.query(&self)
    		.send()
    		.await?
    		.synapse::<PurgeResult>()
    		.await?;
		println!("deleted: {}", res.deleted);
		Ok(())
	}	
}
