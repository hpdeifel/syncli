use crate::*;
use async_trait::async_trait;
use structopt::StructOpt;
use serde::{Deserialize};

#[derive(StructOpt, Debug)]
pub struct VersionCmd {}

#[derive(Deserialize, Debug)]
struct Version {
	server_version: String,
	python_version: String,
}

#[async_trait]
impl Command for VersionCmd {
	async fn run(self, client: &Client) -> Result<(), Box<dyn Error>> {
		let res = client.get_synapse::<Version>(&["v1", "server_version"]).await?;
        println!("server version: {}", res.server_version);
		println!("python version: {}", res.python_version);
		Ok(())
	}	
}
