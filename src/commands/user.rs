use crate::client::ResponseExt;
use crate::*;
use async_trait::async_trait;
use chrono::serde::ts_milliseconds;
use chrono::{DateTime, Utc};
use de::Unexpected;
use serde::{de, Deserialize, Deserializer, Serialize};
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
pub enum UserCmd {
    List(UserListCmd),
    Show(UserShowCmd),
}

#[derive(StructOpt, Serialize, Debug)]
pub struct UserListCmd {
    #[structopt(long)]
    from: Option<u64>,

    #[structopt(long)]
    limit: Option<u64>,

    #[structopt(long)]
    user_id: Option<String>,

    #[structopt(long)]
    guests: Option<bool>,

    #[structopt(long)]
    deactivated: Option<bool>,
}

#[derive(StructOpt, Debug)]
pub struct UserShowCmd {
    id: String,
    /// Include information about recent connections
    #[structopt(long)]
    verbose: bool,
}

#[derive(Deserialize, Debug)]
struct User {
    name: String,
    password_hash: String,
    #[serde(deserialize_with = "bool_from_int")]
    is_guest: bool,
    #[serde(deserialize_with = "bool_from_int")]
    admin: bool,
    #[serde(deserialize_with = "bool_from_int")]
    deactivated: bool,
    displayname: Option<String>,
    threepids: Option<Vec<Threepid>>,
    avatar_url: Option<String>,
}

#[derive(Deserialize, Debug)]
struct Threepid {
    medium: String,
    address: String,
}

#[derive(Deserialize, Debug)]
struct Whois {
    user_id: String,
    devices: Devices,
}

#[derive(Deserialize, Debug)]
struct Devices {
    // Why?
    #[serde(rename(deserialize = ""))]
    sessions: Sessions,
}

#[derive(Deserialize, Debug)]
struct Sessions {
    sessions: Vec<Connections>,
}

#[derive(Deserialize, Debug)]
struct Connections {
    connections: Vec<Connection>,
}

#[derive(Deserialize, Debug)]
struct Connection {
    ip: String,
    #[serde(with = "ts_milliseconds")]
    last_seen: DateTime<Utc>,
    user_agent: String,
}

#[derive(Deserialize, Debug)]
struct UserList {
    users: Vec<User>,
    // why the hell is this a string?
    next_token: Option<String>,
}

#[async_trait]
impl Command for UserCmd {
    async fn run(self, client: &Client) -> Result<(), Box<dyn Error>> {
        match self {
            UserCmd::List(opts) => {
                let res = client
                    .get(&["v2", "users"])
                    .query(&opts)
                    .send()
                    .await?
                    .synapse::<UserList>()
                    .await?;

                let len = res.users.len();

                for user in res.users {
                    println!("{}", user.name);
                }

                if let Some(next) = res.next_token {
                    println!(
                        "{} entries shown. Rerun with --from={} to get the next batch",
                        len, next
                    );
                }
                Ok(())
            }
            UserCmd::Show(show) => {
                let users_res = client
                    .get(&["v2", "users", &show.id])
                    .send()
                    .await?
                    .synapse::<User>()
                    .await?;

                println!("Name: {}", users_res.name);
                if let Some(displayname) = users_res.displayname {
                    println!("Display Name: {}", displayname);
                }
                if let Some(avatar) = users_res.avatar_url {
                    println!("Avatar: {}", avatar);
                }
                println!("Admin: {}", users_res.admin);
                println!("Deactivated: {}", users_res.deactivated);
                println!("Guest: {}", users_res.is_guest);
                println!("Threepids:");
                if let Some(threepids) = users_res.threepids {
                    for threepid in threepids {
                        println!("  - {}: {}", threepid.medium, threepid.address);
                    }
                }

                if show.verbose {
                    let whois_res = client
                        .get(&["v1", "whois", &show.id])
                        .send()
                        .await?
                        .synapse::<Whois>()
                        .await?;

                    println!("Sessions:");
                    for session in whois_res.devices.sessions.sessions {
                        println!("  - Connections:");
                        for con in session.connections {
                            println!("    - ip: {}", con.ip);
                            println!("      last seen: {}", con.last_seen);
                            println!("      user agent: {}", con.user_agent);
                        }
                    }
                }
                Ok(())
            }
        }
    }
}

fn bool_from_int<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    match u8::deserialize(deserializer)? {
        0 => Ok(false),
        1 => Ok(true),
        other => Err(de::Error::invalid_value(
            Unexpected::Unsigned(other as u64),
            &"zero or one",
        )),
    }
}
