mod client;
pub mod commands;

pub use client::{Client, SynapseError, SynapseResponse, ResponseExt};

use async_trait::async_trait;
use std::error::Error;


mod command {
	use crate::*;
	
	#[async_trait]
	pub trait Command {
		async fn run(self, client: &Client) -> Result<(), Box<dyn Error>>;
	}
}

pub use command::Command;

mod config {
	use serde::Deserialize;
	use std::fs;
	use std::path::Path;
	use serde_yaml;
	use reqwest::Url;
	use crate::Error;
	
	#[derive(Deserialize, Debug)]
	pub struct Config {
		access_token: String,
		api_url: Url,
	}

	impl Config {
		pub fn read(path: &Path) -> Result<Self, Box<dyn Error>> {
			let file = fs::File::open(path)?;
			let config = serde_yaml::from_reader(&file)?;
			Ok(config)
		}

		pub fn access_token(&self) -> &str {
			&self.access_token
		}

		pub fn api_url(&self) -> &Url {
			&self.api_url
		}
	}
	
}

pub use config::Config;
