use std::error::Error;
use structopt::{self, StructOpt};
use xdg;
use tokio;
use syncli::*;

#[derive(StructOpt, Debug)]
struct Options {
	#[structopt(subcommand)]
	cmd: Commands,
}

#[derive(StructOpt, Debug)]
enum Commands {
	Version(commands::VersionCmd),
	User(commands::UserCmd),
	Room(commands::RoomCmd),
	PurgeMediaCache(commands::PurgeMediaCacheCmd),
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
	let xdg_dirs = xdg::BaseDirectories::with_prefix("syncli")?;
	let config_path = xdg_dirs.place_config_file("config.yaml")?;
	let config = Config::read(&config_path)?;

	let options = Options::from_args();

	let client = Client::new(config);

	match options.cmd {
		Commands::Version(v) => {
			v.run(&client).await?;
		},
		Commands::User(u) => {
			u.run(&client).await?;
		},
		Commands::Room(r) => {
			r.run(&client).await?;
		},
		Commands::PurgeMediaCache(p) => {
			p.run(&client).await?;
		}
	}
	
	Ok(())
}
